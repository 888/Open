######################################################################
#
#
#           ██████╗  █████╗ ███████╗██╗  ██╗██████╗  ██████╗
#           ██╔══██╗██╔══██╗██╔════╝██║  ██║██╔══██╗██╔════╝
#           ██████╔╝███████║███████╗███████║██████╔╝██║     
#           ██╔══██╗██╔══██║╚════██║██╔══██║██╔══██╗██║     
#           ██████╔╝██║  ██║███████║██║  ██║██║  ██║╚██████╗
#           ╚═════╝ ╚═╝  ╚═╝╚══════╝╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝
#
#
######################################################################

#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return


#PS1='[\u@\h \W]\$ '
################################################################
## prompt; the original is commented out over the #####line
#export PS1="\w >>>"

## vim key bindings in shell:
set -o vi

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend autocd

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls -CFhAs --color=auto'
    alias dir='dir -A  --color=auto'
    alias vdir='vdir -A --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
alias ll='ls  -AlFh --color=auto'
alias la='ls -Ashl --color=auto'
alias l='ls -CF --color=auto'

fi

# colored GCC warnings and errors
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'


alias nano='nvim'
alias vim='nvim'
alias vi='nvim'
alias svim='sudo nvim'
alias mv='mv -i'
alias rm='rm -i'
alias cp='cp -i'
alias arc='cd ~/Archive'
alias de='cd ~/Desktop'
alias dl='cd ~/Downloads'
alias up='sudo pacman -Syyu'
alias in='sudo pacman -S'
alias autoremove='sudo pacman -Qtdq | sudo pacman -Rns'
alias remove='sudo pacman -Rs'
alias yayin='yay -S'
alias yayremove='yay -Rns'
alias yayautoremove='yay -Yc'
alias yayup='yay -Sua'
# Move to the parent folder.
alias ..='cd ..;pwd'
# Move up two parent folders.
alias ...='cd ../..;pwd'
# Move up three parent folders.
alias ....='cd ../../..;pwd'

# switch between shells
# I do not recommend switching default SHELL from bash.
#alias tobash="sudo chsh $USER -s /bin/bash && echo 'Now log out.'"
#alias tozsh="sudo chsh $USER -s /bin/zsh && echo 'Now log out.'"
#alias tofish="sudo chsh $USER -s /bin/fish && echo 'Now log out.'"


# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion ]; then
    . /etc/bash_completion
  fi
fi



qlean

neofetch

####colorscript -e 12

echo 'now in bash'
